# Import the USBCD Module
from usbcdmodule import *

# Time for sleep
import time

# Create an instance of the USBCD module
myusb = USBCDModule()

# Get a list of devices
ids = myusb.GetDevices()

# Spin the carousel of the first device to the first disk
myusb.MoveTo(ids[0], 1)

# Move the Disk to the down position (insert from ejected space)
myusb.GetCDDown(ids[0])
time.sleep(1)

# Turn the LED display off
myusb.TurnLEDOff(ids[0])
time.sleep(1)

# Turn the LED display on
myusb.TurnLEDOn(ids[0])
time.sleep(1)

# Reset the device
myusb.Reset(ids[0])
time.sleep(1)

# Clean Up
myusb.Close()

