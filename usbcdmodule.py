from ctypes import *
import time
import sys

class USBCDModule:
    # Device return statuses
    DEVICE_COMMANDOK = 0
    DEVICE_IDERROR = 1
    DEVICE_BUSY = 2
    DEVICE_UNKNOWERROR = 3

    usbcddll = None
    

    # Callback for when device is added/removed
    CMPFUNC = CFUNCTYPE(c_int, POINTER(c_int))
    def OnCDDeviceChange(self,id):
        # TODO: Add test to see if running process here
        # Reload the device list
        self.GetDevices
        return
    
    # Initialization    
    def __init__(self):
        self.usbcddll = windll.USBCDDLL
        self.usbcddll.InitUSBCDLibrary()
        CMPFUNC = CFUNCTYPE(c_int, POINTER(c_int))
        self.usbcddll.SetCDCallbackProc(self.CMPFUNC(self.OnCDDeviceChange))
        return

    # Retrieve a list of devices
    def GetDevices(self):
        ids = []
        numdevices = self.usbcddll.GetDeviceNumber()
        print("NumDevices: " + str(numdevices))
        for x in range(0, numdevices):
            ids.append(self.usbcddll.EnumDevice(x))
            print("Found: " + str(ids[len(ids)-1]))
        if len(ids) < 1:
            print("No loader found.")
            self.usbcddll.CloseUSBCDLibrary()
            sys.exit(-1)
        return ids

    # Spin the Carousel to the position
    # - Takes time to spin, keep checking status
    def MoveTo(self, id, position):
        self.usbcddll.USBCDMoveto(id, position)
        USBCDStatus = self.usbcddll.USBCDGetStatus(id)
        if USBCDStatus == self.DEVICE_IDERROR:
            print("MoveTo: ID Status Error")
        else:
            print("MoveTo: Moving")
            while USBCDStatus != self.DEVICE_COMMANDOK:
                USBCDStatus = self.usbcddll.USBCDGetStatus(id)    
                time.sleep(1)
            # Sleep 2 seconds to allow tray to eject/insert disk
            time.sleep(2)
        return

    # Move the CD to the down position (eject)
    def GetCDDown(self, id):
        self.usbcddll.USBCDGetCDDown(id)
        USBCDStatus = self.usbcddll.USBCDGetStatus(id)
        if USBCDStatus == self.DEVICE_IDERROR:
            print("GetCDDown: ID Status Error")
        elif USBCDStatus == self.DEVICE_COMMANDOK:
            print("GetCDDown: Moving Down")
            # Sleep 2 seconds to allow tray to eject/insert disk
            time.sleep(2)
        else:
            print("Unknown Error")
            sys.exit(-1)
        return

    # Turn the LED On
    # - Instant Change
    def TurnLEDOn(self, id):
        self.usbcddll.USBCDLEDON(id)
        USBCDStatus = self.usbcddll.USBCDGetStatus(id)
        if USBCDStatus == self.DEVICE_IDERROR:
            print("TurnLEDOn: ID Status Error")
        elif USBCDStatus == self.DEVICE_COMMANDOK:
            print("TurnLEDOn: Turning On")
        else:
            print("Unknown Error")
            sys.exit(-1)
        return

    # Turn the LED Off
    # - Instant Change
    def TurnLEDOff(self, id): 
        self.usbcddll.USBCDLEDOFF(id)
        USBCDStatus = self.usbcddll.USBCDGetStatus(id)
        if USBCDStatus == self.DEVICE_IDERROR:
            print("TurnLEDOff: ID Status Error")
        elif USBCDStatus == self.DEVICE_COMMANDOK:
            print("TurnLEDOff: Turning Off")
        else:
            print("Unknown Error")
            sys.exit(-1)
        return


    # Reset the Device
    # - Instant Change
    def Reset(self, id):
        # Don't reset if there isn't an error - can cause failure / SOS message
        if self.usbcddll.USBCDGetStatus(id) == self.DEVICE_UNKNOWERROR:
            self.usbcddll.USBCDReset(id)
            USBCDStatus = self.usbcddll.USBCDGetStatus(id)
            if USBCDStatus == self.DEVICE_IDERROR:
                print("Reset: ID Status Error")
            elif USBCDStatus == self.DEVICE_COMMANDOK:
                print("Reset: Resetting")
            else:
                print("Unknown Error")
                sys.exit(-1)
        return

    # Close the Library
    # - Instant Change
    def Close(self):
        self.usbcddll.CloseUSBCDLibrary()